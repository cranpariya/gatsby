import React from "react"
import sciencepic from "../images/science.jpg"
import logo from "../images/logo.png"
import Layout from "../components/layout"

import SEO from "../components/seo"


const IndexPage = () => (

  
  <Layout>
    <SEO title="About" keywords={[`gatsby`, `openconcept`, `js`]} />
    <img class="logo" src={logo} alt="logo" />
  
    <p style={{ fontWeight: `600`,color: `#454651`,marginBottom: `0` }}>We develop digital strategies</p>
<p>Give us your problem, we find the solution<br/><span>We work for clients. Through strategy planning, we work with you to identify
and solve digital problems. We find solutions that meet your mandate,
regulatory requirements and fit within your budget.</span></p>

<p style={{ fontWeight: `600`,color: `#454651`,marginBottom: `0` }}>We do accessibility reviews</p>
<p>Your content is important, we make sure everyone can access it<br/><span>We use digital tools and manual investigation to identify compliance with
WCAG requirements as well as accessibility laws in your jurisdiction.
Our report will include your successes, where you fall short, and identify
necessary remediation steps</span></p>

<p style={{ fontWeight: `600`,color: `#454651`,marginBottom: `0` }}>We conceptualize, create and code</p>
<p>We learn about your audience and content managers and design for them<br/><span>It is important to us that people like and understand the design, user
experience (UX), user interface (UI) and information architecture (IA) of a
product before we begin development.</span></p>

<p style={{ fontWeight: `600`,color: `#454651`,marginBottom: `0` }}>We work with the best technology</p>
<p>We make technology easy to use and look after those bits that aren’t<br/><span>We find the technology solution that best meets the requirements of your
digital strategy. We offer hosting as well as maintenance and support
agreements</span></p>

<p style={{ fontWeight: `600`,color: `#454651`,marginBottom: `0` }}>Our approach</p>
<p>We have a dedicated Project Manager to provide the attention you deserve.<br/><span>
Agile project management allows us to engage you in the planning, design,
development, testing and delivery of a product. Not every client has the
resources or needs for that level of engagement. We accommodate whatever
style of planning and reporting works best for you.</span></p>
    
<img src={sciencepic} style={{ marginBottom: `0`,paddingTop:`1rem` }} alt="science" /> 
  </Layout>
)

export default IndexPage
