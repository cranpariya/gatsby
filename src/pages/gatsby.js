import React from "react"


import Layout from "../components/layout"
import SEO from "../components/seo"
import logo from "../images/logo.png"
import code1 from "../images/1.png"
import code2 from "../images/2.png"
// import gatsbypic from "../images/gatsby-astronaut.png"

const GatsbyPage = () => (
  <Layout>
    <SEO title="gatsby" keywords={[`gatsby`, `application`, `react`]} />

  

    
     
      <img class="logo" src={logo} alt="logo" />

      <p style={{ fontWeight: `600`,color: `#454651`,marginBottom: `0` }}>GatsbyJs : a current trend</p>
      <p>Gatsby is a fast modern static site generator for React.</p>

<p><strong>React:</strong> React is library (built with JavaScript) for building user interfaces. Gatsby uses react framework to build static pages content.<br />
<strong>GraphQL:</strong> GraphQL is a query language. It allows you to pull data into your website. It’s the interface that Gatsby uses for managing site data.GraphQL fetch data from sources (WordPress, Drupal and many more) and generates static content.</p>
 
  <p>To get started with Gatsby, you’ll need following software tools installed:</p>
  
    <ul class="inline">
      <li>Node.js</li>
      <li>npm CLI</li>
      <li>Gatsby CLI</li>

    </ul>
    <br/>
    

  <p>When you build your Gatsby application, the output is static content: HTML, CSS, JavaScript, images, etc. This content is incredibly easy and affordable to host with any provider. We have several guides below for hosting with particular providers, but this list is by no means exhaustive</p>   
  
  <p>GatsbyJs has awesome inbuilt features which enhances of overall functionality current web develpmeent.gatsbyJs is serverless and has many features like fastest content delivery, progressive web, faster time interaction, modern code syntax, fater design iterations and <a href ="https://www.gatsbyjs.org/features/">many more</a> </p>   
      
      <h4>Gatsby Link</h4>
      <p>For internal navigation, Gatsby includes a built-in 'Link' component as well as a navigate function which is used for programmatic navigation.Gatsby’s 'Link' component enables linking to internal pages as well as a powerful performance feature called preloading. Preloading is used to prefetch resources so that the resources are fetched by the time the user navigates with this component.</p>   


  <h3>Drupal + Gatsby</h3>
  <p>Drupal as a headless CMS with Gatsby enhances the overall CMS functionality and provides modern web development with additional features.</p>

<p><strong>Implementing Drupal + Gatsby</strong></p>

<ul>
	<li>To pulls data from Drupal 8 sites, <strong> JSONAPI and JSONAPI:Extras</strong> modules need to be installed.</li>
	<li>Install <strong>gatsby-source-drupal</strong> plugin.
    <ul>
      <li>npm install --save gatsby-source-drupal</li>
    </ul>
	</li>


  <li>Configure gatsby-source-drupal plugin in your gatsby-config.js
	<ul>
		<li>
		<img class="code" src={code1} alt="gatsby_config" />
    </li>
	</ul>
	</li>
  
  <li>How to fetch nodes created in Drupal sites  using graphQL queries
	<ul>
		<li>
		<img class="code" src={code2} alt="graphQL" />
    </li>
	</ul>
	</li>

  <li>
<p><strong>GatsbyJS Workflow</strong></p>
<center>
<p class="rtecenter">Data Sources&nbsp;</p>

<p class="rtecenter">||</p>

<p class="rtecenter">Pull data from using GraphQL</p>

<p class="rtecenter">||</p>

<p class="rtecenter">render data to HTML, CSS, React</p>

<p class="rtecenter">||</p>

<p class="rtecenter">Deployed into Static site host</p>

<p class="rtecenter">||</p>

<p class="rtecenter">STATIC SITE HOST</p>
</center>

</li>
  </ul>


  </Layout>

)

export default GatsbyPage
