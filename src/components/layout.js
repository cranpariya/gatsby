/**
 * Layout component that queries for data
 * with Gatsby's StaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"

/*import Header from "./header"*/
import Menu from "./menu"

import drupal from "../images/drupal.png"
import bullfrog from "../images/bullfrog.png"
import bcorp  from "../images/bcorp.png"
import wordpress from "../images/wordpress.png"

import "./layout.css"

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
      <div class="body_container">
            <div class="col-2">
  
        {
          /* <Header siteTitle={data.site.siteMetadata.title} */
          }

        <Menu />
        <div
          style={{
            paddingTop: 0,
          }}
        >
          <main>{children}</main>
        
          </div>
         
        </div>

        <nav class="col-1">
      <p class="intro">We are an Ottawa based digital agency with over twenty years of experience.</p>
      
      <p class="intro">We work in partnership
with you to deliver digital
products that exceed
expectations.</p>

<h3>Our Values</h3>

<p>We support the open source
software community
<br/><span>More secure, reliable and
economical than proprietary
software, open source
democratizes participation in
digital society. We are one of the
top Canadian contributors to
the open source community.</span></p>
<p>We work to make the web
accessible<br/><span>
The right to participate in a
digital society must include all
people, regardless of ability,
socio-economic status, gender,
race or geography. We are
internationally recognized
experts on accessibility.</span></p>
<p>We build communities<br/>
<span>We support our employees, our
community and our environment.
We are a certified B-corporation,
a member of Carbon613,
support several non-profits
and have donated space for
community organizers.</span></p>

<img class="navlogo" src={drupal} style={{ width: `70%` }} alt="science" />
<img class="navlogo" src={bullfrog} style={{ width: `70%` }} alt="science" />
<img class="navlogo" src={bcorp} style={{ width: `30%` }} alt="science" />
<img  class="navlogo"src={wordpress} style={{ width: `70%` }} alt="science" />
      </nav>

      
        </div>
        <footer>
          <div class="web_container">
             Built using
            {` `}
            <a href="https://www.gatsbyjs.org">GatsbyJs</a>
            </div>
          </footer>
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
