import React from 'react'
import Link from 'gatsby-link'



const activestyle = {

 color:'#ffffff',

}
const Menu = () => (

    <div
    style={{
      background: '#fff'
    
    }}
  >
     <ul
      style={{
        listStyle: 'none',
        listStyleType: 'none',
        display: 'flex',
        justifyContent: 'space-evenly',
        wordBreak: 'break-word',
        marginLeft:'0',
        marginBottom:'0',
        padding:'2rem',
        fontSize:'1rem',
      background: '#333',
     

      }}
    >
      <li>
        <Link activeStyle={activestyle} to="/">About Us</Link>
      </li>
      <li>
        <Link activeStyle={activestyle} to="/gatsby">GatsbyJS</Link>
      </li>
   
    
    </ul>


 <ul class="second_nav">
  <li>
  <span>openconcept.ca</span>
  </li>
  <li>
  <span>info@openconcept.ca</span>
  </li>
  <li>
  <span>613.686.6736</span>
  </li>

</ul>
</div>
  
)

export default Menu